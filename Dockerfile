# Keep distro in sync with CI builder! We run perf inside the container, needs to work with host kernel.
FROM debian:buster
RUN rm /bin/sh && ln -s /bin/bash /bin/sh

# apt package installation
# gmp: needed by Coq
# python, python3-distutils: needed by z3 (for RefinedC)
# mpfr: needed by Cerberus (for RefinedC)
# pkg-config: needed by the conf-mpfr opam package (related to the use of mpfr by Cerberus)
RUN apt update -y && apt install -y pkg-config git rsync tar unzip m4 time curl linux-perf strace ocaml build-essential bubblewrap gawk libgmp-dev python2.7 python3 python3-distutils libmpfr-dev libssl-dev

# opam installation
RUN curl "https://github.com/ocaml/opam/releases/download/2.1.5/opam-2.1.5-x86_64-linux" -Lo /usr/local/bin/opam && chmod +x /usr/local/bin/opam

# prepare CI environment
WORKDIR /
RUN useradd -m ci
USER ci

# rustup installation
RUN curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs > /tmp/rustup && sh /tmp/rustup -y
RUN source "$HOME/.cargo/env" && cargo install rustup-toolchain-install-master



