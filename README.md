# rust-ci

This is a version of the Iris CI (https://gitlab.mpi-sws.org/iris/ci) including `rustup` with `rustup-toolchain-install-master` for providing CI runners for projects including Rust code.
